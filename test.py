import unittest
from unittest.mock import Mock
import re

mock_addword = Mock()
mock_addword.side_effect=["aTWENTYoneletterwords","counterrevolutionists",""]
words=[]
addedwords=[]
start=""
target=""
mandword=""

class teststring: 
    def start_target(self,start,target):
        file = open("dictionary.txt")                       #opening the dictionary that we must use
        lines = file.readlines()
        global words
        while True:                                   #this loop is to correct user input(all alphabetic and lowercase) 
            while True:
                start = start.casefold()
                if start.isalpha():
                  break
                else:
                  print("please enter only alphabetical letters")



            for line in lines:                              #selects from the dictionary all the words that are the length of user input 
                word = line.rstrip()
                if len(word) == len(start):
                  words.append(word)
              
            while True:                                       #this loop is to correct user input(all alphabetic and lowercase)
                #target = dummyinput("Enter target word:")
                target = target.casefold()
                if target.isalpha():
                  break
                else:
                  print("please enter only letters of the alphabet")

            if len(target) != len(start) :                  #this is to make user input conform to the laddergram rules i.e all words are the same length
                print("the words must be the same length")
                continue
            break
        global pass_target_to_other_func
        pass_target_to_other_func = target
        file.close()
        return start,target,words

    def addwords_words(self,addword):
        target=pass_target_to_other_func
        global words
        while True:                                       #this loop is so the user can enter more words in the dictionary for this round, input will be corrected(all alphabetic and lowercase)     
            addword = addword.casefold()
            if addword == "":
                for removedoubles in filter(lambda x: x in addedwords, words):
                    addedwords.remove(removedoubles) 
                words.extend(addedwords)
                break
            elif len(target) != len(addword) :
                print("the words must be the same length")
                continue
            else:
                if addword.isalpha():
                    addedwords.append(addword)
                else:
                    print("please enter only letters of the alphabet")
        return words

    def mandword_chk(self,mandword):
        target=pass_target_to_other_func
        global words
        while True:                                       #this loop is for entering a word from the dictionary that will be mandatory in the laddergram path for this round, input will be corrected(all alphabetic and lowercase)
            #mandword = dummyinput("Enter a word from the dictionary that must be necessary to forming a path to the target word or just leave blank to continue:")
            mandword = mandword.casefold()
            if mandword == "":
                break
            elif len(target) != len(mandword) :
              print("the words must be the same length")
              continue
            else:
              if mandword.isalpha() and mandword in words:
                break
              elif mandword.isalpha()==False:
                print("please enter only letters of the alphabet") 
              else:
                print("please enter a word that is either in the dictionary or one that you have added to it")

        return mandword
    
class testmainfunction:
    def __init__(self,start):
        self.words=[]
        self.seen=dict(start=True)
        self.path=[start]
        file = open("dictionary.txt")                       #opening the dictionary that we must use
        lines = file.readlines()
        for line in lines:                              #selects from the dictionary all the words that are the length of user input 
            word = line.rstrip()
            if len(word) == len(start):
                self.words.append(word)
        file.close()
        
    def same(self,item, target):                             #this takes 2 words and returns how many letters are the same and in the same position
        return len([itemword for (itemword, targetword) in zip(item, target) if itemword == targetword])

    def build(self,pattern, words, seen):                    #creates a list of words that are different by one letter from the given word out of the 'words' list but have not already been used
        return [word for word in words
                     if re.search(pattern, word) and word not in seen.keys()]

    def find(self, word, words, seen, target, path):          #this function finds the target by changing global variables seen and path and returns true or false
        roundlist = []
        for i in range(len(word)):
            roundlist += self.build(word[:i] + "." + word[i + 1:], words, seen)
        if len(roundlist) == 0:                                #this means that the current word has no other word in the dictionary that is different by one letter that hasn't already been used in previous iterations of this loop
            return False                                      
        roundlist = sorted([(self.same(w, target), w) for w in roundlist],reverse=True)
        for (match, item) in roundlist:                        #trying to find a word that is closest to the target word
            if match >= len(target):
                path.append(item) 
                return True
            seen[item] = True
        for (match, item) in filter(lambda x:x[0] >= self.same(path[-1],target),roundlist): 
            path.append(item)           
            if self.find(item, words, seen, target, path):       #recursive use of the function, the item in the list closest to the target becomes the word to then be input in the same function
                return True
            path.pop()
        return False                                      #if the function has arrived at the end then the dictionary has been exhausted and there is no path to the target word

    
class Testme(unittest.TestCase):

    def setUp(self):
        self.test = teststring()
        self.test2 = testmainfunction("lead")
        self.test3 = testmainfunction("hide")
          
    def test_letters(self):
        answer = self.test.start_target('incomprehensibiliTIES', 'IndIstInguIshableness')
        self.assertEqual(answer,  ('incomprehensibilities', 'indistinguishableness',['compartmentalizations','counterrevolutionists','establishmentarianism','incomprehensibilities', 'indistinguishableness','ultraminiaturizations']))
        answer2 = self.test.mandword_chk('comPARTmentalizations')
        self.assertEqual(answer2, 'compartmentalizations' )
        answer3 = self.test.mandword_chk("")
        self.assertEqual(answer3, "" )
        answer4 = self.test.addwords_words(mock_addword())
        self.assertEqual(answer4, ['compartmentalizations','counterrevolutionists','establishmentarianism','incomprehensibilities', 'indistinguishableness','ultraminiaturizations','atwentyoneletterwords'])

    @unittest.skip
    def test_digits(self):
        self.skiptest("this will be a contiuous loop")
        answer = self.test.start_target('3%!*?', '42086')
        self.assertEqual(answer,  "please enter only alphabetical letters")
        answer2 = self.test.mandword_chk('21$\l:gerty45701:>871')
        self.assertEqual(answer2, 'please enter only letters of the alphabet' )
        answer3 = self.test.addwords_words('21$\l:gerty45701:>871')
        self.assertEqual(answer3,  "please enter only letters of the alphabet")
        
    @unittest.skip
    def test_not_same_length(self):
        self.skiptest("this will be a contiuous loop")
        answer = self.test.start_target('lead', 'goalie')
        self.assertEqual(answer,  "the words must be the same length")
        answer2 = self.test.mandword_chk('truck')
        self.assertEqual(answer2, 'the words must be the same length')
        answer3 = self.test.addwords_words('bat')
        self.assertEqual(answer3, 'the words must be the same length')

    @unittest.skip
    def test_mandword_notin_dict(self):
        self.skiptest("this will be a contiuous loop")
        answer = self.test.mandword_chk('wordisnotindictionary')
        self.assertEqual(answer, 'please enter a word that is either in the dictionary or one that you have added to it' )
        
    def test_lead_gold(self):
        answer = self.test2.find('lead',self.test2.words,self.test2.seen, 'gold',self.test2.path)
        self.assertTrue(answer)
        self.assertEqual(self.test2.path,['lead', 'load', 'goad', 'gold'])

    def test_hide_seek(self):
        answer = self.test3.find('hide',self.test3.words,self.test3.seen, 'seek',self.test3.path)
        self.assertTrue(answer)
        self.assertEqual(self.test3.path,['hide', 'side', 'site', 'sits', 'sies', 'sees', 'seek'])

if __name__ == '__main__':
    unittest.main()

      
