import re                                           #the regex library needed for the build function


def same(item, target):                             #this takes 2 words and returns how many letters are the same and in the same position
  return len([c for (c, t) in zip(item, target) if c == t])

def build(pattern, words, seen):                    #creates a list of words that are different by one letter from the given word out of the 'words' list but have not already been used
  return [word for word in words
                 if re.search(pattern, word) and word not in seen.keys()]

def find(word, words, seen, target, path):          #this function finds the target by changing global variables seen and path and returns true or false
  list = []
  for i in range(len(word)):
    list += build(word[:i] + "." + word[i + 1:], words, seen)
  if len(list) == 0:                                #this means that the current word has no other word in the dictionary that is different by one letter that hasn't already been used in previous iterations of this loop
    return False                                      
  list = sorted([(same(w, target), w) for w in list],reverse=True)
  for (match, item) in list:                        #trying to find a word that is closest to the target word
    if match >= len(target):
      path.append(item) 
      return True                                   #True is only returned if a path is found toward the target word
    seen[item] = True
  for (match, item) in filter(lambda x:x[0] >= same(path[-1],target),list): 
    path.append(item)           
    if find(item, words, seen, target, path):       #recursive use of the function, the item in the list closest to the target becomes the word to then be input in the same function
      return True
    path.pop()
  return False                                      #if the function has arrived at the end then the dictionary has been exhausted and there is no path to the target word

##########################################################function definitions over#############################################

file = open("dictionary.txt")                       #opening the dictionary that we must use
lines = file.readlines()
while True:                                         #begining the loop so that the user can continue to play the laddergram game one after the other

  while True:                                       #this loop is to correct user input(all alphabetic and lowercase) 
    start = input("Enter start word:")
    start = start.casefold()
    if start.isalpha():
      break
    else:
      print("please enter only alphabetical letters ")
   
  words = []                                      #reset the values from last round
  addedwords = []
  
  for line in lines:                              #selects from the dictionary all the words that are the length of user input 
    word = line.rstrip()
    if len(word) == len(start):
      words.append(word)
      
  while True:                                       #this loop is to correct user input(all alphabetic and lowercase)
    target = input("Enter target word:")
    target = target.casefold()
    if target.isalpha():
      break
    else:
      print("please enter only letters of the alphabet")

  if len(target) != len(start) :                  #this is to make user input conform to the laddergram rules i.e all words are the same length
    print("the words must be the same length")
    continue

  while True:                                       #this loop is so the user can enter more words in the dictionary for this round, input will be corrected(all alphabetic and lowercase)
    addword = input("Enter any word you would like to add the dictionary for this round or just leave blank to continue:")
    addword = addword.casefold()
    if addword == "":
      for removedoubles in filter(lambda x: x in addedwords, words):
        addedwords.remove(removedoubles)       
      words.extend(addedwords)
      break
    elif len(target) != len(addword) :
      print("the words must be the same length")
      continue
    else:
      if addword.isalpha():
        addedwords.append(addword)
      else:
        print("please enter only letters of the alphabet")    

  while True:                                       #this loop is for entering a word from the dictionary that will be mandatory in the laddergram path for this round, input will be corrected(all alphabetic and lowercase)
    mandword = input("Enter a word from the dictionary that must be necessary to forming a path to the target word or just leave blank to continue:")
    mandword = mandword.casefold()
    if mandword == "":
        break
    elif len(target) != len(mandword) :
      print("the words must be the same length")
      continue
    else:
      if mandword.isalpha() and mandword in words:
        break
      elif mandword.isalpha()==False:
        print("please enter only letters of the alphabet") 
      else:
        print("please enter a word that is either in the dictionary or one that you have added to it")


####################################################user input over###########################################################

  path = [start]
  seen = {start : True}
  if find(start, words, seen, target, path):
    print(len(path) - 1, path)
  else:
    print("No path found")


#change dictionary to a list? 
